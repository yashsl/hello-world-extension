webpackHotUpdate("app",{

/***/ "./src/pages/Signup.js":
/*!*****************************!*\
  !*** ./src/pages/Signup.js ***!
  \*****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var _jsxFileName = "D:\\YASH\\repos\\my-browser-extension\\src\\pages\\Signup.js";

/* global chrome*/

/* eslint-disable no-undef */
 //import './SignUp.css'

class SignUp extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(...args) {
    super(...args);
    this.state = {
      search: ""
    };

    this.changeHandler = (e, fieldName) => {
      this.setState({
        [fieldName]: e.target.value
      });
    };

    this.showLocal = (val = this.state.search) => {
      chrome.windows.getCurrent(function (currentWindow) {
        chrome.tabs.query({
          active: true,
          windowId: currentWindow.id
        }, function () {
          var searchBox = document.getElementsByClassName("search-global-typeahead__input"); //searchBox.click();

          alert(1);
          searchBox.value = `${val}`;
        });
      });
    };
  }

  render() {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 35,
        columnNumber: 7
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
      placeholder: "Search",
      type: "text",
      onChange: e => this.changeHandler(e, "search"),
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 36,
        columnNumber: 9
      }
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
      onClick: this.showLocal,
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 41,
        columnNumber: 9
      }
    }, "Local"));
  }

}

/* harmony default export */ __webpack_exports__["default"] = (SignUp);
/* eslint-enable no-undef */

/***/ })

})
//# sourceMappingURL=app.8a21130a1aa0001d52c5.hot-update.js.map