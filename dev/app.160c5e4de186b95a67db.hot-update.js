webpackHotUpdate("app",{

/***/ "./src/App.js":
/*!********************!*\
  !*** ./src/App.js ***!
  \********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_chrome_extension_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-chrome-extension-router */ "./node_modules/react-chrome-extension-router/dist/index.js");
/* harmony import */ var react_chrome_extension_router__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_chrome_extension_router__WEBPACK_IMPORTED_MODULE_2__);
var _jsxFileName = "D:\\YASH\\repos\\my-browser-extension\\src\\App.js";
// import React, { Component } from "react";
// import { Route, Switch, withRouter } from "react-router-dom";
// //import { Container, Button, TextField, Typography } from "@material-ui/core";
// import SignUp from './pages/Signup';
// import SignIn from './pages/SignIn';
// import "./App.css";
// class App extends Component {
//   state = {
//     message: "",
//   };
//   changeHandler = (e) => {
//     this.setState({ message: e.target.value });
//   };
//   submitHandler = () => {
//     alert(`Name Entered is: ${this.state.message}`);
//   };
//   render() {
//     return (
//       <div className="App">
//         <Switch>
//           <Route path='/' component={SignUp} />
//           <Route path='/signIn' component={SignIn} />
//         </Switch>
//       </div>
//     );
//   }
// }
// export default withRouter(App);




const Three = ({
  message
}) => /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
  onClick: () => Object(react_chrome_extension_router__WEBPACK_IMPORTED_MODULE_2__["popToTop"])(),
  __self: undefined,
  __source: {
    fileName: _jsxFileName,
    lineNumber: 46,
    columnNumber: 3
  }
}, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
  __self: undefined,
  __source: {
    fileName: _jsxFileName,
    lineNumber: 47,
    columnNumber: 5
  }
}, message), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
  __self: undefined,
  __source: {
    fileName: _jsxFileName,
    lineNumber: 48,
    columnNumber: 5
  }
}, "Click me to pop to the top"));

const Two = ({
  message
}) => /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
  __self: undefined,
  __source: {
    fileName: _jsxFileName,
    lineNumber: 53,
    columnNumber: 3
  }
}, "This is component Two. I was passed a message:", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
  __self: undefined,
  __source: {
    fileName: _jsxFileName,
    lineNumber: 55,
    columnNumber: 5
  }
}, message), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
  onClick: () => Object(react_chrome_extension_router__WEBPACK_IMPORTED_MODULE_2__["goBack"])(),
  __self: undefined,
  __source: {
    fileName: _jsxFileName,
    lineNumber: 56,
    columnNumber: 5
  }
}, "Click me to go back to component One"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
  onClick: () => Object(react_chrome_extension_router__WEBPACK_IMPORTED_MODULE_2__["goTo"])(Three, {
    message
  }),
  __self: undefined,
  __source: {
    fileName: _jsxFileName,
    lineNumber: 59,
    columnNumber: 5
  }
}, "Click me to go to component Three!"));

const One = () => {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_chrome_extension_router__WEBPACK_IMPORTED_MODULE_2__["Link"], {
    component: Two,
    props: {
      message: "I came from component one!"
    },
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 67,
      columnNumber: 5
    }
  }, "This is component One. Click me to route to component Two");
};

const App = () => {
  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(() => {
    setInterval(() => {
      const {
        component,
        props
      } = Object(react_chrome_extension_router__WEBPACK_IMPORTED_MODULE_2__["getCurrent"])();
      console.log(component ? `There is a component on the stack! ${component} with ${props}` : `The current stack is empty so Router's direct children will be rendered`);
      const components = Object(react_chrome_extension_router__WEBPACK_IMPORTED_MODULE_2__["getComponentStack"])();
      console.log(`The stack has ${components.length} components on the stack`);
    }, 500);
  });
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_chrome_extension_router__WEBPACK_IMPORTED_MODULE_2__["Router"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 87,
      columnNumber: 5
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(One, {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 88,
      columnNumber: 7
    }
  }));
};

/* harmony default export */ __webpack_exports__["default"] = (App);

/***/ })

})
//# sourceMappingURL=app.160c5e4de186b95a67db.hot-update.js.map