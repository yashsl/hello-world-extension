/* global chrome*/

import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Button } from "@material-ui/core";

//import './SignUp.css'

class SignUp extends Component {
  state = {
    name: "",
    email: "",
    password: "",
  };

  changeHandler = (e, fieldName) => {
    this.setState({ [fieldName]: e.target.value });
  };

  submitHandler = () => {
    alert(
      `Name: ${this.state.name}\nEmail: ${this.state.email}\nPassword: ${this.state.password}`
    );
  };

  showLocal = () => {
    //alert(chrome)
    // chrome.storage.local.set({ key: 1 }, function () {
    //   alert("Value is set to " + 1);
    // });
    // chrome.windows.getCurrent((currentWindow) => {
    //   chrome.tabs.query(
    //     { active: true, windowId: currentWindow.id },
    //     (activeTabs) => {
    //       // inject content_script to current tab
    //       console.log(currentWindow);
    //       alert("This is a chrome api script.");
    //     }
    //   );
    // });

    chrome.tabs.executeScript({
      code: 'document.body.style.backgroundColor="red"',
    });
  };

  render() {
    return (
      <div className="signup-form">
        <header>
          <h1>Hello there!</h1>
        </header>
        <form onSubmit={this.submitHandler}>
          <label htmlFor="name">Name</label>
          <input
            placeholder="Name"
            type="text"
            id="name"
            onChange={(e) => this.changeHandler(e, "name")}
          />
          <label htmlFor="email">Email</label>
          <input
            placeholder="Email"
            type="email"
            onChange={(e) => this.changeHandler(e, "email")}
          />
          <label htmlFor="password">Password</label>
          <input
            placeholder="Password"
            type="password"
            onChange={(e) => this.changeHandler(e, "password")}
          />
          <button>Sign Up</button>
          <Link to="/signIn">Sign in</Link>
        </form>
        <Button color="primary" onClick={this.showLocal}>
          Test btn
        </Button>
      </div>
    );
  }
}

export default SignUp;
