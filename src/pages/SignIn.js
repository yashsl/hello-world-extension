import React, { Component } from "react";
import { Link } from "react-router-dom";

class SignIn extends Component {
  render() {
    return (
      <div>
        This is signin page
        <Link to="/signUp">Sign In</Link>
      </div>
    );
  }
}

export default SignIn;
