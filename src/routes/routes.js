import React from "react";
import { MemoryRouter as Router, Route, Redirect } from "react-router-dom";
//import { createBrowserHistory } from "history";

import SignUp from "../pages/Signup";
import SignIn from "../pages/SignIn";

//const history = createBrowserHistory();
//in future if doesnt work, add history={history} in Router as Prop

class Routes extends React.Component {
  render() {
    return (
      <div>
        <Router>
          <Redirect exact from="/" to="/signUp" />
          <Route exact path="/signUp" component={SignUp} />
          <Route exact path="/signIn" component={SignIn} />
        </Router>
      </div>
    );
  }
}

export default Routes;
